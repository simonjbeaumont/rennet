IMG_NAME=selenium/standalone-chrome-debug:3.4.0
CON_NAME=textspaced-driver
VIRTUALENV=.venv
PYTHON_FILES=rennet.py

all: python

run: stop
	docker run --name=$(CON_NAME) -d -p 4444:4444 -p 5900:5900 $(IMG_NAME)

stop:
	docker rm -f $(CON_NAME) 2>/dev/null || true

restart: stop run

shell:
	docker exec -it `docker ps -q -f name=$(CON_NAME)` /bin/bash

vnc:
	open vnc://localhost:5900

purge: stop
	@read -n1 -r -p "This will remove the docker image. Are you sure? " ;\
	echo ;\
	if [ "$$REPLY" == "y" ]; then \
		docker rmi $(IMG_NAME) 2>/dev/null || true; \
	fi

$(VIRTUALENV): requirements.txt
	virtualenv $(VIRTUALENV)
	$(VIRTUALENV)/bin/pip install -r requirements.txt

python: $(VIRTUALENV)
	$(VIRTUALENV)/bin/pep8 $(PYTHON_FILES)
	$(VIRTUALENV)/bin/pyflakes $(PYTHON_FILES)
	$(VIRTUALENV)/bin/pylint --rcfile pylintrc $(PYTHON_FILES)

test: python

clean:
	rm -rf $(VIRTUALENV)

.PHONY: all purge python run shell stop shell test vnc
