# Rennet [![Build status][ci-build-badge]][ci-build-status]

A simple bot for TextSpaced.

### Requirements

* Docker
* Python
* Virtualenv

### Usage

This makes use of a self-contained Selenium webdriver which is backed by
Docker. For convenience its use is wrapped in a Makefile.

To start the webdriver container:

```sh
$ make run
```

To stop the webdriver container:

```sh
$ make stop
```

The bot logic is then run in a Python virtualenv. To create the virtualenv:

```sh
$ make python
```

You can then run the bot in the virtualenv. To see a list of supported
sub-commands and command line options:

```sh
$ .venv/bin/python rennet.py --help
```

### Development

The `make test` target has all the checks that are run on the CI so you can
validate your changes before opening pushing.

[ci-build-badge]: https://gitlab.com/simonjbeaumont/rennet/badges/master/build.svg
[ci-build-status]: https://gitlab.com/simonjbeaumont/rennet/commits/master
