# -*- coding: utf-8 -*-
"""
Update sieve mail rules on LDAP server.
"""

import argparse
import getpass
import logging
import os
import random
import signal
import subprocess
import sys
import time
import traceback

from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import WebDriverException
from selenium.webdriver.common.keys import Keys

DEFAULT_DRIVER_ADDR = "http://127.0.0.1:4444/wd/hub"

FACEBOOK_URL = "https://www.messenger.com/t/TextSpaced"

KEYCHAIN_SERVICE = "textspaced-bot"
LOGGER_NAME = __file__


BUTTON_BUY = "Buy"
BUTTON_BUY_BOOKS = "Buy Books"
BUTTON_BUY_PART_CRATES = "Buy Part Crates"
BUTTON_CANCEL = "Cancel"
BUTTON_CARGO = "Cargo"
BUTTON_CORES = "Cores"
BUTTON_CRAFTING = "Crafting"
BUTTON_CRAFT_PORT = "Craft Port"
BUTTON_DOCK = "Dock"
BUTTON_HELM = "Helm"
BUTTON_INVENTORY = "Inventory"
BUTTON_LAUNCHERS = "Launchers"
BUTTON_MERCHANT_BASE = "Merchant Base"
BUTTON_MERCHANT_STATION = "Merchant Station"
BUTTON_MERGE = "Merge"
BUTTON_MODIFIERS = "Modifiers"
BUTTON_MOUNTS = "Mounts"
BUTTON_MY_STATS = "My Stats"
BUTTON_PORTS = "Ports"
BUTTON_PORT_CRAFTING = "Port Crafting"
BUTTON_PORT_CRAFTING_MODULE = "Port Crafting Module"
BUTTON_SELL_PORTS = "Sell Ports"
BUTTON_SETTINGS = "Settings"
BUTTON_SORT_ACCURACY_POSTFIX = "Accuracy"
BUTTON_SORT_PREFIX = "Sort by "
BUTTON_SORT_QUANTITY_POSTFIX = "Quantity"
BUTTON_TACTICAL = "Tactical"
BUTTON_UNDOCK = "Undock"
BUTTON_USE = "Use"
BUTTON_VIEW_PARTS = "View Parts"
BUTTON_VIEW_RECIPE = "View Recipe"
BUTTON_YES = "Yes"
RESPONSE_NO_FREE_PORTS = "You have no free ports"

XPATH_FB_LOGIN_INPUT_EMAIL = "//input[@id='email']"
XPATH_FB_LOGIN_INPUT_PASS = "//input[@id='pass']"
XPATH_FB_LOGIN_BUTTON = "//button[@id='loginbutton']"
XPATH_FB_NAVIGATION = "//div[@role='navigation']"
XPATH_FB_SEND_A_LIKE = "//a[@role='button'][@title='Send a Like']"
XPATH_FB_SCROLLABLE_LIST = (
    "//div[@direction='forward']"
    "/ancestor::div[contains"
    "(concat(' ', normalize-space(@class), ' '), ' direction_ltr ')]")
XPATH_FB_MENU_BUTTONS = (
    "//div[@role='presentation']"
    "/following-sibling::div"
    "//div[@role='button']")
XPATH_FB_RESPONSES = (
    "//div[contains(concat(' ', normalize-space(@class), ' '), ' clearfix ')]"
    "/div/div/span")
XPATH_FB_MESSAGE_BOX = (
    "//div[@aria-label='New message']//div[@contenteditable='true']")


FTL_DIRECTION_FACTOR = {
    "positive": 1,
    "negative": -1,
}


def logger():
    return logging.getLogger(LOGGER_NAME)


def execute(command_line, sensitive=False, suppress_stderr=True):
    logger().debug("Execute: `%s`",
                   command_line if not sensitive else "[redacted]")
    cmd = command_line.split()
    if suppress_stderr:
        with open(os.devnull, 'w') as devnull:
            return subprocess.check_output(cmd, stderr=devnull)
    return subprocess.check_output(cmd)


def get_password_from_keychain(service, account):
    cmd = ("security find-generic-password -s {service} -a {account} -w"
           .format(service=service, account=account))
    return execute(cmd).strip()


def add_password_to_keychain(service, account, password):
    cmd = ("security add-generic-password -s {service} -a {account} -w {pw}"
           .format(service=service, account=account, pw=password))
    return execute(cmd, sensitive=True)


def prompt_for_confirmation(message):
    sys.stdout.write("{} [y/N] ".format(message))
    while True:
        choice = raw_input()
        if choice.lower() == "y":
            return True
        elif choice.lower() == "n" or choice == "":
            return False
        sys.stdout.write("Please respond with 'y' or 'n' (case insensitive)")


def get_password_from_keychain_or_prompt(service, account):
    try:
        return get_password_from_keychain(service, account)
    except subprocess.CalledProcessError:
        pass
    password = getpass.getpass("Facebook password for {}: ".format(account))
    if prompt_for_confirmation("Store in keychain for future use?"):
        add_password_to_keychain(service, account, password)
    return password


def random_delay(begin, end):
    delay = random.uniform(begin, end)
    now = time.time()
    logger().debug("Delaying for random time U(%s, %s): %s", begin, end, delay)
    logger().info(
        "Sleeping until %s",
        time.strftime("%H:%M:%S %Z", time.localtime(now + delay)))
    time.sleep(delay)


def find_by_xpath(driver, xpath, timeout=10, with_delay=True):
    if with_delay:
        random_delay(3, 5)
    logger().debug("Looking for elements with xpath: %s", xpath)
    wait = WebDriverWait(driver, timeout)
    try:
        wait.until(EC.visibility_of_element_located((By.XPATH, xpath)))
    except webdriver.support.wait.TimeoutException:
        pass
    elems = driver.find_elements_by_xpath(xpath)
    logger().debug("Found %d elements with xpath %s", len(elems), xpath)
    return elems


def populate_input_with_xpath(driver, xpath, value):
    elem = find_by_xpath(driver, xpath)[0]
    elem.clear()
    elem.send_keys(value)
    return elem


def send_message(driver, message):
    elem = populate_input_with_xpath(driver, XPATH_FB_MESSAGE_BOX, message)
    elem.send_keys(Keys.ENTER)


def get_button_text(button):
    return button.get_attribute("textContent")


def really_click(driver, elem):
    """
    Uses javascript to click an element which may not be visible. Invisible
    elements cannot be clicked using webdriver.
    """
    logger().debug("Injecting javascript to click element: (%s, %s)",
                   elem.id, get_button_text(elem))
    driver.execute_script('arguments[0].click();', elem)


def get_menu_buttons(driver):
    buttons = find_by_xpath(driver, XPATH_FB_MENU_BUTTONS)
    logger().debug("Found %d menu buttons: %s",
                   len(buttons),
                   [(b.id, get_button_text(b)) for b in buttons])
    return buttons


def find_menu_buttons_with_label(driver, label):
    return [b for b in get_menu_buttons(driver)
            if get_button_text(b) == label]


def click_menu_button_with_label(driver, label):
    logger().info("Clicking button with label: %s", label)
    really_click(driver, find_menu_buttons_with_label(driver, label)[0])


def find_scrollable_lists(driver):
    lists = find_by_xpath(driver, XPATH_FB_SCROLLABLE_LIST)
    logger().debug("Found %d scrollable lists", len(lists))
    return lists


def find_buttons_in_scrollable_list_by_text(scrollable_list, text):
    xpath = ".//a[text()='{}']".format(text)
    buttons = scrollable_list.find_elements_by_xpath(xpath)
    logger().debug("Found %d buttons with text %s in scrollable_list %s",
                   len(buttons), text, scrollable_list)
    return buttons


def get_driver(driver_addr, session_id=None):
    driver = webdriver.Remote(
        command_executor=driver_addr,
        desired_capabilities=DesiredCapabilities.CHROME)
    if session_id:
        logger().info("Attempting to reconnect to session: %s", session_id)
        driver.close()
        driver.session_id = session_id
    else:
        logger().info("New webdriver session: %s", driver.session_id)
    return driver


def reset_menus(driver):
    logger().info("Resetting menus")
    find_by_xpath(driver, XPATH_FB_SEND_A_LIKE)[0].click()
    if find_menu_buttons_with_label(driver, BUTTON_UNDOCK):
        click_menu_button_with_label(driver, BUTTON_UNDOCK)


def is_logged_in(driver):
    url_is_correct = driver.current_url == FACEBOOK_URL
    logger().debug("URL is expected: %s", url_is_correct)
    nav_loaded = len(find_by_xpath(driver, XPATH_FB_NAVIGATION)) > 0
    logger().debug("Navigation has loaded: %s", nav_loaded)
    logged_in = url_is_correct and nav_loaded
    logger().info("Existing login detected: %s", logged_in)
    return logged_in


def login(driver, user):
    logger().info("Logging in as %s", user)
    password = get_password_from_keychain_or_prompt(KEYCHAIN_SERVICE, user)
    driver.get(FACEBOOK_URL)
    assert "Messenger" in driver.title
    populate_input_with_xpath(driver, XPATH_FB_LOGIN_INPUT_EMAIL, user)
    populate_input_with_xpath(driver, XPATH_FB_LOGIN_INPUT_PASS, password)
    find_by_xpath(driver, XPATH_FB_LOGIN_BUTTON)[0].click()
    logger().info("Successfully logged in")


def merge_parts_of_kind_single_pass(driver, kind):
    reset_menus(driver)
    click_menu_button_with_label(driver, BUTTON_CRAFTING)
    click_menu_button_with_label(driver, BUTTON_PORT_CRAFTING)
    click_menu_button_with_label(driver, kind)
    click_menu_button_with_label(
        driver, BUTTON_SORT_PREFIX + BUTTON_SORT_QUANTITY_POSTFIX)
    last_list = find_scrollable_lists(driver)[-1]
    merge_buttons = find_buttons_in_scrollable_list_by_text(
        last_list, BUTTON_MERGE)
    merge_count = 0
    for button in merge_buttons:
        random_delay(2, 5)
        really_click(driver, button)
        merge_count += 1
    logger().info("Merged %d parts of kind: %s", merge_count, kind)
    return merge_count


def merge_all_parts_of_kind(driver, kind):
    parts_merged_total = 0
    while True:
        parts_merged = merge_parts_of_kind_single_pass(driver, kind)
        parts_merged_total += parts_merged
        if parts_merged == 0:
            break
    logger().info("Finished merging all parts; total merged: %d",
                  parts_merged_total)


def merge_all_parts(driver):
    for kind in (BUTTON_CORES, BUTTON_LAUNCHERS,
                 BUTTON_MODIFIERS, BUTTON_MOUNTS):
        logger().info("Merging parts of kind: %s", kind)
        merge_all_parts_of_kind(driver, kind)


def fill_recipe(driver, sort_by):
    reset_menus(driver)
    click_menu_button_with_label(driver, BUTTON_CRAFTING)
    click_menu_button_with_label(driver, BUTTON_PORT_CRAFTING)
    for kind in (BUTTON_CORES, BUTTON_LAUNCHERS,
                 BUTTON_MODIFIERS, BUTTON_MOUNTS):
        click_menu_button_with_label(driver, kind)
        click_menu_button_with_label(driver, BUTTON_SORT_PREFIX + sort_by)
        last_list = find_scrollable_lists(driver)[-1]
        use_buttons = find_buttons_in_scrollable_list_by_text(
            last_list, BUTTON_USE)
        best_use_button = use_buttons[0]
        really_click(driver, best_use_button)
        click_menu_button_with_label(driver, BUTTON_VIEW_PARTS)


def no_free_ports(driver):
    reset_menus(driver)
    click_menu_button_with_label(driver, BUTTON_CRAFTING)
    click_menu_button_with_label(driver, BUTTON_PORT_CRAFTING)
    click_menu_button_with_label(driver, BUTTON_VIEW_RECIPE)
    responses = get_recent_responses(driver, limit=10)
    return len([r for r in responses
                if RESPONSE_NO_FREE_PORTS in r]) > 0


def craft_port(driver, sort_by):
    if no_free_ports(driver):
        raise Exception("Cannot craft, no free ports")
    logger().info("Crafting port: parts sorted by: %s", sort_by)
    fill_recipe(driver, sort_by)
    reset_menus(driver)
    click_menu_button_with_label(driver, BUTTON_CRAFTING)
    click_menu_button_with_label(driver, BUTTON_PORT_CRAFTING)
    click_menu_button_with_label(driver, BUTTON_VIEW_RECIPE)
    click_menu_button_with_label(driver, BUTTON_CRAFT_PORT)


def craft_all_ports(driver, sort_by):
    ports_crafted = 0
    while not no_free_ports(driver):
        craft_port(driver, sort_by)
        ports_crafted += 1
    logger().info("Finished crafting: %d ports crafted", ports_crafted)


def is_docked(driver):
    return find_menu_buttons_with_label(driver, BUTTON_UNDOCK)


def dock_with_merchant(driver):
    reset_menus(driver)
    click_menu_button_with_label(driver, BUTTON_HELM)
    click_menu_button_with_label(driver, BUTTON_DOCK)
    if find_menu_buttons_with_label(driver, BUTTON_MERCHANT_BASE):
        logger().info("Attempting to dock with merchant base")
        click_menu_button_with_label(driver, BUTTON_MERCHANT_BASE)
        logger().info("Docked with merchant base")
    elif find_menu_buttons_with_label(driver, BUTTON_MERCHANT_STATION):
        logger().info("Attempting to dock with merchant station")
        click_menu_button_with_label(driver, BUTTON_MERCHANT_STATION)
        logger().info("Docked with merchant station")
    else:
        logger().info("Couldn't find merchant to dock with")
        raise Exception("Could not dock")


def get_recent_responses(driver, limit=10):
    # TODO: could using get_attribute(textContent) simplify this xpath
    elems = find_by_xpath(driver, XPATH_FB_RESPONSES)[-limit:]
    responses = [elem.text for elem in elems if elem.text != ""]
    responses = responses[-limit:]
    responses.reverse()
    logger().debug("Found %d recent responses (limit %d): %s",
                   len(responses), limit, responses)
    return responses


def is_latest_offer_profitable(driver):
    responses = get_recent_responses(driver, limit=10)
    try:
        rrp_response = [r for r in responses if "RRP" in r][0]
    except IndexError:
        raise Exception("Could not find RRP response")
    profit = rrp_response.split("RRP")[0].split()[-1]
    if not profit.endswith("%"):
        raise Exception("Could not extract %% from RRP message")
    if profit.startswith("-"):
        return False
    if profit.startswith("+"):
        return True
    raise Exception("Could not extract +/- from RRP message")


def sell_port(driver, port_name):
    if not is_docked(driver):
        dock_with_merchant(driver)
    click_menu_button_with_label(driver, BUTTON_SELL_PORTS)
    click_menu_button_with_label(driver, port_name)
    random_delay(5, 7)
    if is_latest_offer_profitable(driver):
        logger().info("Selling port for a profit: %s", port_name)
        click_menu_button_with_label(driver, BUTTON_YES)
    else:
        logger().info("Not selling port for a loss: %s", port_name)
        click_menu_button_with_label(driver, BUTTON_CANCEL)


def get_current_ports(driver):
    reset_menus(driver)
    click_menu_button_with_label(driver, BUTTON_TACTICAL)
    click_menu_button_with_label(driver, BUTTON_PORTS)
    current_ports = [get_button_text(b) for b in get_menu_buttons(driver)
                     if get_button_text(b) != BUTTON_CANCEL]
    logger().debug("Current ports: %s", current_ports)
    return current_ports


def item_in_list_fuzzy(item, l):
    return any([item in i or i in item for i in l])


def sell_all_ports(driver, exclusions):
    exclusions.append(BUTTON_PORT_CRAFTING_MODULE)
    ports_to_sell = []
    for port in get_current_ports(driver):
        if item_in_list_fuzzy(port, exclusions):
            logger().info("Excluding port from sale: %s", port)
        else:
            ports_to_sell.append(port)
    for port in ports_to_sell:
        logger().info("Selling port: %s", port)
        sell_port(driver, port)


def buy_parts(driver, budget):
    logger().info("Spending %d credits on parts")
    if not is_docked(driver):
        dock_with_merchant(driver)
    click_menu_button_with_label(driver, BUTTON_BUY_PART_CRATES)
    last_list = find_scrollable_lists(driver)[-1]
    buy_buttons = find_buttons_in_scrollable_list_by_text(
        last_list, BUTTON_BUY)
    first_buy_button = buy_buttons[0]
    num_crates_to_buy = budget / 2000
    for i in range(0, num_crates_to_buy):
        random_delay(3, 5)
        really_click(driver, first_buy_button)
        logger().info("Bought %d of %d common crates", i+1, num_crates_to_buy)


# pylint: disable=too-many-arguments
def buy_craft_sell(driver, budget, merge, sort_by, exclusions,
                   sell_current, repeat):
    for _ in range(0, repeat):
        if not sell_current:
            exclusions += get_current_ports(driver)
        buy_parts(driver, budget)
        if merge:
            merge_all_parts(driver)
        craft_all_ports(driver, sort_by)
        sell_all_ports(driver, exclusions)


def max_ftl(driver, direction):
    send_message(driver, "Ftl")
    random_delay(3, 5)
    responses = get_recent_responses(driver, limit=7)
    ftl_response = [r for r in responses if "Your light drive" in r][0]
    charge = ftl_response.split("LYs")[0].split()[-1]
    distance = FTL_DIRECTION_FACTOR[direction] * int(charge)
    logger().info("Traveling %s LYs", distance)
    send_message(driver, "Ftl {}".format(distance))


def explore(driver):
    logger().info("Exploring sector")
    elem = find_by_xpath(driver, XPATH_FB_MESSAGE_BOX)[0]
    elem.clear()
    elem.send_keys("Helm_explore")
    elem.send_keys(Keys.ENTER)


def check_stats(driver):
    reset_menus(driver)
    click_menu_button_with_label(driver, BUTTON_SETTINGS)
    click_menu_button_with_label(driver, BUTTON_MY_STATS)


def check_inventory(driver):
    reset_menus(driver)
    click_menu_button_with_label(driver, BUTTON_CARGO)
    click_menu_button_with_label(driver, BUTTON_INVENTORY)


def buy_books(driver):
    reset_menus(driver)
    # TODO: dock with both kinds of merchant
    if not is_docked(driver):
        dock_with_merchant(driver)
    click_menu_button_with_label(driver, BUTTON_BUY_BOOKS)
    # TODO: parse the button menu for buy buttons and click


# pylint: disable=too-few-public-methods
class open_webdriver_and_login(object):
    def __init__(self, args):
        self.args = args
        self.driver = None

    def __enter__(self):
        self.driver = get_driver(self.args.driver, self.args.session_id)
        if not is_logged_in(self.driver):
            login(self.driver, self.args.user)
        return self.driver

    def __exit__(self, _type, _value, _traceback):
        if not self.driver:
            logger().warn("Webdriver session not initialised")
            return
        if self.args.keep_open:
            logger().info("Keeping webdriver alive, session: %s",
                          self.driver.session_id)
            return
        logger().info("Closing webdriver session")
        self.driver.close()


def login_command(args):
    open_webdriver_and_login(args)


def merge_parts_command(args):
    with open_webdriver_and_login(args) as driver:
        merge_all_parts(driver)


def dock_command(args):
    with open_webdriver_and_login(args) as driver:
        dock_with_merchant(driver)


def sell_ports_command(args):
    with open_webdriver_and_login(args) as driver:
        sell_all_ports(driver, args.exclude)


def buy_parts_command(args):
    with open_webdriver_and_login(args) as driver:
        buy_parts(driver, args.budget)


def craft_ports_command(args):
    with open_webdriver_and_login(args) as driver:
        craft_all_ports(driver, args.sort_by)


def buy_craft_sell_command(args):
    with open_webdriver_and_login(args) as driver:
        buy_craft_sell(
            driver, args.budget, args.merge, args.sort_by,
            args.exclude, args.sell_current, args.repeat)


def max_ftl_command(args):
    with open_webdriver_and_login(args) as driver:
        logger().info(
            "Performing max FTL (%s) %s times", args.direction, args.repeat)
        reset_menus(driver)
        error_last_time = False
        for _ in range(0, args.repeat):
            try:
                max_ftl(driver, args.direction)
                error_last_time = False
                random_delay(15*60, 20*60)
            except Exception as e:  # pylint: disable=broad-except
                if error_last_time:
                    logger().error("Repeated error... exiting: %s", type(e))
                    raise e
                logger().warn("Ignoring exception and continuing: %s", type(e))
                error_last_time = True
                reset_menus(driver)


def parse_args_or_exit(argv=None):
    parser = argparse.ArgumentParser(
        description="Bot for TextSpaced",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    # common arguments
    parser.add_argument("--driver", default=DEFAULT_DRIVER_ADDR,
                        help="Address of selenium webdriver server")
    parser.add_argument("--session-id",
                        help="ID of existing webdriver session")
    parser.add_argument("--keep-open", action="store_true",
                        help="Don't shutdown driver on exit")
    parser.add_argument("--user", required=True,
                        help="Facebook username")
    parser.add_argument("--backtrace", action="store_true",
                        help="Print backtrace to stdout on error")
    logging_group = parser.add_mutually_exclusive_group()
    logging_group.add_argument(
        "--debug", "-v", action="store_true", help="Enable debug logging")
    logging_group.add_argument(
        "-q", action="count", dest="quiet",
        help="Only error logging (-qq for no logging)")
    subparsers = parser.add_subparsers()
    # login command
    subparser = subparsers.add_parser("login", help="Just login")
    subparser.set_defaults(func=login_command)
    # dock command
    subparser = subparsers.add_parser("dock", help="Dock with merchant")
    subparser.set_defaults(func=dock_command)
    # merge-parts command
    subparser = subparsers.add_parser("merge-parts", help="Merge all parts")
    subparser.set_defaults(func=merge_parts_command)
    # sell-all-ports command
    subparser = subparsers.add_parser("sell-ports", help="Sell all ports")
    subparser.add_argument("--exclude", nargs="+", default=[],
                           help="Exclude parts from sale")
    subparser.set_defaults(func=sell_ports_command)
    # buy-parts command
    subparser = subparsers.add_parser("buy-parts", help="Buy part crates")
    subparser.add_argument("--budget", type=int, required=True,
                           help="How much to spend on crates")
    subparser.set_defaults(func=buy_parts_command)
    # craft-ports command
    subparser = subparsers.add_parser("craft-ports", help="Craft ports")
    subparser.add_argument("--sort-by", default=BUTTON_SORT_ACCURACY_POSTFIX,
                           help="Choose parts based on best of this criterion")
    subparser.set_defaults(func=craft_ports_command)
    # buy-merge-craft-sell-ports command
    subparser = subparsers.add_parser("buy-craft-sell-ports", help="££")
    subparser.add_argument("--budget", type=int, required=True,
                           help="How much to spend on crates")
    subparser.add_argument("--merge", action="store_true",
                           help="Merge parts before crafting")
    subparser.add_argument("--sort-by", default=BUTTON_SORT_ACCURACY_POSTFIX,
                           help="Choose parts based on best of this criterion")
    subparser.add_argument("--exclude", nargs="+", default=[],
                           help="Exclude ports from sale")
    subparser.add_argument("--sell-current", action="store_true",
                           help="Sell existing ports as well")
    subparser.add_argument("--repeat", type=int, default=1,
                           help="Run in a loop...")
    subparser.set_defaults(func=buy_craft_sell_command)
    # max-ftl command
    subparser = subparsers.add_parser("max-ftl", help="")
    subparser.add_argument("direction", choices=FTL_DIRECTION_FACTOR.keys(),
                           help="Direction of travel in space")
    subparser.add_argument("--repeat", type=int, default=1,
                           help="Run in a loop...")
    subparser.set_defaults(func=max_ftl_command)
    return parser.parse_args(argv)


def setup_logging(args):
    level = logging.INFO
    if args.debug:
        level = logging.DEBUG
    elif args.quiet == 1:
        level = logging.WARN
    elif args.quiet == 2:
        level = logging.ERROR
    elif args.quiet > 2:
        level = logging.NOTSET
    datefmt = '%Y-%m-%d %H:%M:%S %Z'
    fmt = ("[%(asctime)s %(levelname)5s "
           "%(filename)s:%(funcName)s#L%(lineno)d]: %(message)s")
    logging.basicConfig(format=fmt, datefmt=datefmt, level=logging.ERROR)
    logging.getLogger(LOGGER_NAME).setLevel(level)
    logger().debug("Logging initialised")


def main(argv):
    args = parse_args_or_exit(argv)
    setup_logging(args)
    try:
        args.func(args)
    except WebDriverException as e:
        sys.stderr.write("error: Webdriver call failed...\n")
        sys.stderr.write("error: {}\n".format(type(e)))
        sys.stderr.write("error: {}\n".format(e))
    except Exception as e:  # pylint: disable=broad-except
        sys.stderr.write("error: {}\n".format(type(e)))
        if args.backtrace:
            traceback.print_exc()
        else:
            sys.stderr.write("error: For backtrace, run with --backtrace\n")
        sys.exit(1)


if __name__ == "__main__":
    signal.signal(signal.SIGINT, lambda x, _: sys.exit(128 + x))
    main(sys.argv[1:])
